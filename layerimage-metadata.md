# Layerimage Metadata Specification

This specification describes the file format of the Layer Image Metadata that is stored as JSON file inside of the `resources/layers` folder.

### Directory Structure

Each image file inside the `layers` folder must be accompanied by one or multiple JSON files.
The first JSON file must have the exact same file name as the image file, with its extension changed to lower-case `json`.
Each following file, if multiple files are to be specified, must match the glob pattern `{name}.*.json`, where `{name}` is the name without extension of the image file.
Afterwards, each layer image is referenced by the name of its metadata file without extension.

### File Structure

Each metadata file must be a valid JSON file with a dictionary at its root.

##### Required fields

The dictionary must contain the following entries:

* *source* - The source URL of the image
* *author* - The name of the author or license holder
* *license* - The name of the license of the image
* *licensetext* - The full license text
* *transform* - A JSON List of transformations, see [transformations](#transformations)

### <a name="transformations"></a>Transformations

Each transformation is specified by a JSON dictionary mapping a key to a value.

##### Rotate
```json
{
"rotate": <angle>
}
```
Rotates the whole image by *angle*, resizing the image if necessary.

##### Brightness
```json
{
"brightness": <brightness>
}
```
Changes the brightness of the image, `1.0` means an unchanged image.

##### Contrast
```json
{
"contrast": <contrast>
}
```
Changes the contrast of the image, `1.0` means an unchanged image.

##### Greyscale Conversion
```json
{
"blackandwhite": "yes"
}
```
Converts the whole image to greyscale