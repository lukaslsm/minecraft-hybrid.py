# Contributing

Everyone is welcome to contribute to this project or change the code to match different requirements.
Just fork the project and commit your changes.
If you think everyone could benefit from your changes, feel free to submit a merge request!

## How does the build.py script work?

The build.py script only reads the JSON metadata for blocks (in blocks.json) and layer images (in the respective JSON file).
It processes all images that are specified in those JSON files and creates a new texture pack.
Therefore, if you only want to add new blocks, e.g. for mods, you likely only need to edit the blocks.json file.

## Adding a new Layer Image

All layer images must have a file format that can be processed by Pillow (preferrably JPEG or PNG) and must be placed inside the `resources/layers` folder.
Each layer image file must be accompanied by a json metadata file that has the same file name as the layer image file.
The detailled metadata format specification can be seen [here](layerimage-metadata.md).

## Adding a new Block Specification

All Block Specifications must be added to the global [blocks.json](blocks.json) file. 
A detailled reference of this file can be found [here](blocks-metadata.md)

## Tips & Tricks

To easily get a list of all blocks of a specific kind that can be pasted into a JSON file, e.g. to get a list of all leaves, you can use the following command inside the assets folder:
```bash
for e in $(ls -1 | grep leaves | cut -f 1 -d '.'); do echo \"$e\",; done
```