"""
This file contains the layer file logic used to process layer files
"""
# MIT License
#
# Copyright © 2020-2021 Lukas Kirschner
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import sys
from pathlib import Path
from typing import Dict, List, NoReturn

from PIL import Image, ImageEnhance


class LayerFile:
    def __init__(self, sourcefile: Path, metadata: Path, resolution: int):
        """
        Load a layer image, apply all transformations from the metadata file and resize the image such that it fits exactly into the texture pack size.
        :param sourcefile: Layer image file
        :param metadata: Metadata file
        :param resolution: Resolution of the texture pack
        """
        with metadata.open("r") as jsonfile:
            meta: Dict = json.load(jsonfile)
            try:
                self.source: str = meta["source"]
                self.license: str = meta["license"]
                self.licensetext: str = meta["licensetext"]
                self.author: str = meta["author"]
                self.transform: List[Dict[str, str]] = meta["transform"]
                self.resolution: int = resolution
            except NameError as e:
                print(f"Invalid metadata found in {metadata}!")
                raise e
        self.image: Image = Image.open(sourcefile)
        for transformation in self.transform:
            if "rotate" in transformation:
                self.image = self.image.rotate(int(transformation["rotate"]), expand=True)
            if "blackandwhite" in transformation:
                bw: bool = transformation["blackandwhite"] == "yes"
                if (bw):
                    self.image = self.image.convert("LA")
                    self.image = self.image.convert("RGBA")
            if "contrast" in transformation:
                ctr = transformation["contrast"]
                contrast = ImageEnhance.Contrast(self.image)
                self.image = contrast.enhance(float(ctr))
            if "brightness" in transformation:
                br = transformation["brightness"]
                brightness = ImageEnhance.Brightness(self.image)
                self.image = brightness.enhance(float(br))
        self._crop_me(self.resolution,
                      self.resolution)  # If the resolution doesn't exactly match with the resource pack resolution, crop me
        self.image = self.image.convert('RGBA')

    def _crop_me(self, new_width: int, new_height: int) -> NoReturn:
        """
        Crop the image such that it exactly fits the texture pack size. Resizes the image proportionally first.
        Image MUST be greater or equal as the given resolution
        :param new_width: New width
        :param new_height: New height
        :return: None
        """
        width, height = self.image.size  # Get dimensions
        if width == new_width and height == new_height:
            return
        if height > width:
            percent = (new_width / float(self.image.size[0]))
            hnew = self.image.size[1] * percent
            wnew = new_width
        else:
            percent = (new_height / float(self.image.size[1]))
            wnew = self.image.size[0] * percent
            hnew = new_height
        self.image = self.image.resize((int(wnew), int(hnew)), Image.ANTIALIAS)
        width, height = self.image.size  # Get dimensions
        assert width >= new_width, f"The layer image must be at least as large as the texture pack's dimensions! Width was {width}, expected at least {new_width}"
        assert height >= new_height, f"The layer image must be at least as large as the texture pack's dimensions! Height was {height}, expected at least {new_height}"
        left = (width - new_width) / 2
        top = (height - new_height) / 2
        right = (width + new_width) / 2
        bottom = (height + new_height) / 2
        self.image = self.image.crop((left, top, right, bottom))

    # Required Properties
    @property
    def get_source(self) -> str:
        """
        :return: the source URL of the file
        """
        return self.source

    @property
    def get_license(self) -> str:
        """
        :return: the license of the file
        """
        return self.license

    @property
    def get_licensetext(self) -> str:
        """
        :return: the license text of the file
        """
        return self.licensetext

    @property
    def get_author(self) -> str:
        """
        :return: the author's name of the file
        """
        return self.author

    @property
    def get_resolution(self) -> int:
        """
        :return: the author's name of the file
        """
        return self.resolution

    @property
    def get_image(self) -> Image:
        """
        :return: the RGBA image, which is the same resolution as the texture pack itself
        """
        return self.image


if __name__ == "__main__":
    sys.exit(1)
