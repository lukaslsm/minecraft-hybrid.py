"""
This file contains transformations that can be applied to a source image
"""
# MIT License
#
# Copyright © 2020-2021 Lukas Kirschner
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy
from PIL import Image
from blend_modes import blending_functions


class Transformations:
    @staticmethod
    def blend(img1: Image, img2: Image, opacity: str, mixmode: str, **kwargs):
        """
        Blend two images using the given parameters.
        Expects both images to have the same size.
        :param img1: First image
        :param img2: Second image
        :param opacity: Opacity of the blend
        :param mixmode: Mix Mode
        :param kwargs:
        :return: the new image
        """
        image: Image
        if mixmode == "normal":
            image = Image.blend(img1, img2, alpha=float(opacity) / 100.0)
        else:
            img1_arr = numpy.array(img1)
            img1_arr = img1_arr.astype(float)
            img2_arr = numpy.array(img2)
            img2_arr = img2_arr.astype(float)
            if mixmode == "soft_light":
                image_arr = blending_functions.soft_light(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "hard_light":
                image_arr = blending_functions.hard_light(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "lighten_only":
                image_arr = blending_functions.lighten_only(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "darken_only":
                image_arr = blending_functions.darken_only(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "multiply":
                image_arr = blending_functions.multiply(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "difference":
                image_arr = blending_functions.difference(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "subtract":
                image_arr = blending_functions.subtract(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "grain_extract":
                image_arr = blending_functions.grain_extract(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "grain_merge":
                image_arr = blending_functions.grain_merge(img1_arr, img2_arr, float(opacity) / 100.0)
            elif mixmode == "divide":
                image_arr = blending_functions.divide(img1_arr, img2_arr, float(opacity) / 100.0)
            else:
                raise Exception(f"Illegal Mix Mode: {mixmode}")
            image_arr = numpy.uint8(image_arr)
            image = Image.fromarray(image_arr)

        return image
