"""
build.py - Build a Minecraft Hybrid Texture Pack
"""
# MIT License
#
# Copyright © 2020-2021 Lukas Kirschner
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import argparse
import copy
import json
import logging
import math
import sys
from logging import Logger
from pathlib import Path
from typing import Set, Dict, Union, List, Any, Optional, NoReturn

from PIL import Image
from tqdm import tqdm

from src.layerfile import LayerFile
from src.transformations import Transformations

logging.getLogger('PIL').setLevel(logging.INFO)  # Disable annoying log messages

resolution: int = 256

metadata_file: str = \
    """{
       "pack": {
          "pack_format": 6,
          "description": "Minecraft Hybrid Texture Pack"
       }
    }"""


def get_animated(mcmeta: dict[str, Any]) -> Optional[dict[str, Any]]:
    if "animation" in mcmeta:
        return mcmeta["animation"]
    else:
        return None


def resize_nearestneighbor(img: Image, w: int, h: int, mcmeta: dict[str, Any]) -> Image:
    """
    Resize an image with the Nearest Neighbor Interpolation.
    If the image is an animation, use the proportional height instead of the given height
    :param img: Image to resize
    :param w: Desired Width
    :param h: Desired Height
    :param mcmeta:
    :return:
    """
    if img.width == w and img.height == h:
        return img
    if get_animated(mcmeta) is None:
        return img.resize((w, h), Image.NEAREST)
    else:
        # TODO Do we need to check something inside the dict returned from mcmeta?
        mul: int = img.height // img.width
        assert mul * img.width == img.height, f"The height {img.height} is not divisible by the width {img.width}!"
        img_height: int = mul * w
        # h is ignored for animated textures!
        return img.resize((w, img_height), Image.NEAREST)


def combine_images_vertically(images: list[Image]) -> Image:
    """
    Combine multiple images with the same width vertically
    :param images: Images to combine
    :return: the new combined image
    """
    assert images is not None, f"No list of images was given!"
    assert len(images) > 0, f"An empty list of images was given!"
    if len(images) == 1:  # No need to combine anything
        return images[0]
    pos: int = 0
    width: int = images[0].width
    ret = Image.new('RGBA', (width, sum(i.height for i in images)))
    for image in images:
        assert image.width == width, f"One of the images had a wrong width of {image.width} (expected {width})"
        ret.paste(image, (0, pos))
        pos += image.height
    return ret


def split_image_to_rectangles(image: Image) -> list[Image]:
    width, height = image.size
    upper = 0
    left = 0
    slices = height // width
    assert slices * width == height, f"The height {height} is not divisible by the width {width}!"
    ret: list[Image] = []

    for count in range(1, slices + 1):
        # if we are at the end, set the lower bound to be the bottom of the image
        if count == slices:
            lower = height
        else:
            lower = int(count * width)
        newslice = image.crop((left, upper, width, lower))
        upper += width
        ret.append(newslice)
    return ret


def load_layerimages(logger: Logger, resources: Path) -> dict[str, LayerFile]:
    # Load Layer Images
    logger.info(f"Loading Layer Images...")
    layer_images: Path = resources / "layers"
    layerimages: Dict[str, LayerFile] = dict()
    layerfiles = [i for i in layer_images.iterdir() if (not i.name.endswith(".json")) and i.is_file()]
    for layerimagefile in tqdm(layerfiles, desc="Loading layer images", unit=" files", file=sys.stdout,
                               total=len(layerfiles)):
        name: str = ".".join(layerimagefile.name.split(".")[:-1])
        meta_files: List[Path] = [layer_images / f"{name}.json"]
        meta_files.extend(layer_images.glob(f"{name}.*.json"))
        for meta_file in meta_files:
            meta_name = ".".join(meta_file.name.split(".")[0:-1])
            layerimages[meta_name] = LayerFile(layerimagefile, meta_file, resolution)
    return layerimages


def load_presets(logger, presets_file) -> dict[str, list[dict[str, dict[str, str]]]]:
    logger.info(f"Loading presets...")
    with presets_file.open("r") as pf:
        ret = json.load(pf)
        logger.info(f"Loaded {len(ret)} presets.")
        return ret


def main() -> int:
    global metadata_file, resolution
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG, stream=sys.stdout)
    local: Path = Path(__file__).parent.absolute()
    assets: Path = local / "assets"
    textures: Path = assets / "minecraft" / "textures"
    out_dir: Path = local / "out"
    resources: Path = local / "resources"
    logger: Logger = logging.getLogger("build")

    parser = argparse.ArgumentParser(prog=__file__, description='Minecraft Hybrid Texture Pack Generator')
    parser.add_argument('--minecraft-only', required=False, default=False, action='store_true', dest="minecraft_only",
                        help='Only generate textures for the Minecraft base game. Ignore all Block Metadata'
                             'supplied in the mods/ directory')
    args = parser.parse_args()

    minecraft_only: bool = args.minecraft_only

    if not assets.is_dir():
        logger.critical(f"Could not find assets folder at {assets}!")
        return 1
    if not textures.is_dir():
        logger.critical(f"Could not find Base Game textures folder at {textures}!")
        return 1
    if not out_dir.is_dir():
        out_dir.mkdir(exist_ok=True)
    meta_path = out_dir / "pack.mcmeta"
    with meta_path.open("w") as m:
        m.write(metadata_file)
        del metadata_file
    pack_logo = out_dir / "pack.png"
    src_logo = local / "logo" / "pack.png"
    pack_image = Image.open(src_logo)
    pack_image.save(pack_logo)

    layerimages: dict[str, LayerFile] = load_layerimages(logger, resources)

    presets: dict[str, list[dict[str, dict[str, str]]]] = load_presets(logger, local / "presets.json")

    # Build Base Game Textures
    build_textures_for(logger, "minecraft", assets, layerimages, local, out_dir / "assets", resolution, presets)

    mods: list[str] = [f.name.removesuffix(".json") for f in (local / "mods").iterdir() if f.is_file()]
    for mod in mods:
        if not (assets / mod).is_dir():
            logger.warning(f"Skipping {mod} because the assets are missing.")
            continue
        build_textures_for(logger, mod, assets, layerimages, local, out_dir / "assets", resolution, presets)


def search_all_blocks_in(searchdir: Path, search_root_dir: Path) -> set[str]:
    """
    Recursively search all blocks inside the given directory and all subdirectories.
    All png files are considered a block texture file.
    :param searchdir: Dir to search
    :param search_root_dir: Root dir of the search, MUST be a parent of searchdir!
    :return: a set of all blocks found inside the directory
    """
    ret = set()  # TODO
    for texturefile in searchdir.iterdir():
        if texturefile.is_dir():
            ret = ret.union(search_all_blocks_in(texturefile, search_root_dir))
        elif texturefile.is_file() and texturefile.name.lower().endswith(".png"):
            ret.add(texturefile.relative_to(search_root_dir).as_posix().removesuffix(".png"))
    return ret


def build_textures_for(logger: Logger, modname: str, assets_folder: Path,
                       layerimages: dict[str, LayerFile], local: Path, out_folder: Path, resolution: int,
                       presets: dict[str, list[dict[str, dict[str, str]]]]) -> NoReturn:
    textures: Path = assets_folder / modname / "textures"
    # Load blocks
    logger.info(f"Building Block List for {modname}...")
    blocks: Path = textures / "block"
    blocklist: set[str] = search_all_blocks_in(blocks, blocks)
    logging.info(f"Found {len(blocklist):d} unique blocks")

    # Load Blocks Dict
    if modname == "minecraft":
        blocksdictfile = local / "blocks.json"
    else:
        blocksdictfile = local / "mods" / f"{modname}.json"
    blocksdict: list[dict[str, Union[list, str]]]
    with blocksdictfile.open("r") as bdf:
        blocksdictx = json.load(bdf)
        blocksdict = list()
        # Transform each transform_dict with a list as scope into a single transform_dict
        #  to make tqdm and processing easier
        for transform_dict in blocksdictx:
            for scopeblock in transform_dict["scope"]:
                d = copy.deepcopy(transform_dict)
                d["scope"] = scopeblock
                blocksdict.append(d)

    # Process Blocks
    out_blocks: Path = out_folder / modname / "textures" / "block"
    out_blocks.mkdir(exist_ok=True, parents=True)
    for transform_dict in tqdm((x for x in blocksdict), file=sys.stdout, unit=" blocks",
                               desc=f"{modname} - Processing Block JSON",
                               total=len(blocksdict)):
        block: str = transform_dict["scope"]  # Required
        process: list = transform_dict.setdefault("process", list())  # Not required
        blockfile: Path = blocks / Path(f"{block}.png")  # Path(...) to handle subdirectories
        blockmcmeta: Path = blocks / Path(f"{block}.png.mcmeta")  # Load mcmeta of texture
        if blockmcmeta.is_file():
            with blockmcmeta.open("r") as mcm:
                mcmeta: dict[str, Any] = json.load(mcm)
        else:
            mcmeta: dict[str, Any] = dict()
        image = Image.open(blockfile).convert('RGBA')
        image = resize_nearestneighbor(image, resolution, resolution, mcmeta)
        if block not in blocklist:
            logger.critical(f"Could not find block {block} in available source textures "
                            f"or block {block} has been defined twice!")
            sys.exit(1)
        blocklist.remove(block)  # Remove block from the list of blocks that have not yet been processed
        image = apply_transformations(process, layerimages, image,
                                      mcmeta, presets)  # Apply all transformations from the metadata
        outblockfile = out_blocks / Path(f"{block}.png")
        outblockmcmeta = out_blocks / Path(f"{block}.png.mcmeta")
        # logger.info(f"Saving {outblockfile}")
        outblockfile.parent.mkdir(exist_ok=True, parents=True)
        image.save(outblockfile)
        if len(mcmeta) > 0:  # Write mcmeta to output folder, if it is not empty
            with outblockmcmeta.open("w") as mcm:
                json.dump(mcmeta, mcm, indent=1)

    # Output warning for missing blocks. Output sorted by reversed string, such that logs, planks,
    # etc can be displayed together. This makes it easier to paste them into JSON
    if len(blocklist) > 0:
        logger.warning(f"No metadata was specified for the following {modname} blocks:\n" + '\n'.join(
            f"\"{s}\"," for s in sorted(blocklist, key=texture_sorter)))  # Output as JSON-paste-able data


def texture_sorter(s: str) -> tuple[int, str, Optional[str]]:
    """
    Sorts a texture in such a way that it can be easily batch-copy-pasted into JSON
    :param s: Texture to sort
    :return: a key
    """
    plist = s.split("/")
    spl: list[str] = plist[-1].split("_")
    if len(plist) > 1:
        ind = 0
        return ind, "/".join(plist[:-1]), "_".join(spl[::-1])
    elif len(spl) == 1:
        ind = 2
        return ind, spl[0], None
    return 1, spl[-1], "_".join(spl[:-1:-1])


def apply_transformations(process: list[dict], layerimages: dict, image: Image, mcmeta: dict,
                          presets: dict[str, list[dict[str, dict[str, str]]]]) -> Image:
    """
    Apply all transformations to the given image in-place.
    If an image has an animation, treat every frame of the animation as single image
    and apply the exact same transformations to it.
    :param presets: Dictionary of all available Presets
    :param process: The process dictionary from the block metadata
    :param layerimages: The dictionary of all layerimages
    :param image: The image to transform in-place
    :param mcmeta: The mcmeta metadata of the block itself
    :return: None
    """
    # Great TODO: Animated textures?!?
    if get_animated(mcmeta) is not None:
        images = split_image_to_rectangles(image)
    else:
        images = [image]
    for i in range(len(images)):
        for transformation in process:
            if "blend" in transformation:
                blend = transformation["blend"]
                layerimage = blend["layerimage"]
                layer: LayerFile = layerimages[layerimage]
                images[i] = Transformations.blend(images[i], layer.image, **blend)
            elif "preset" in transformation:
                preset = transformation["preset"]
                preset_name = preset["name"]
                assert preset_name in presets, f"Preset {preset_name} was not found in presets!"
                images[i] = apply_transformations(presets[preset_name], layerimages, images[i], {}, presets)
    image = combine_images_vertically(images)
    return image


if __name__ == "__main__":
    exit(main())
