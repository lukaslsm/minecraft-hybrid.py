#!/bin/bash
set -e

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <full path of the mod jarfile>"
    exit 1
fi

JARPATH=$1

if [ ! -d "assets" ]; then
  echo "Error - the assets folder does not exist in the current working directory!"
  exit 1
fi

if [ ! -f "$JARPATH" ]; then
    echo "$JARPATH does not exist!."
    exit 1
fi

if ! command -v unzip &> /dev/null
then
    echo "unzip was not found on your system! Please install zip and try again."
    exit 1
fi

echo "Extracting assets..."
unzip "${JARPATH}" 'assets/**/textures/block/*' -d . || echo "Could not extract ${JARPATH}, unzip returned non-zero status code $?."
echo "Extraction done."
exit 0