# Minecraft Hybrid.py Texture Pack Generator

**[Issue Tracker](https://gitlab.com/lukaslsm/minecraft-hybrid.py/-/issues)**
| **[Download latest build](https://gitlab.com/lukaslsm/minecraft-hybrid.py/-/jobs/artifacts/master/browse?job=release_pack)**
| **[Download latest nightly build](https://gitlab.com/lukaslsm/minecraft-hybrid.py/-/jobs/artifacts/master/browse?job=deploy_nightly_pack)**

This repository contains a set of python scripts designed to automatically build a Minecraft Hybrid pack out of
high-resolution textures and original textures.

#### Is this just another texture pack?

No, this is not a texture pack, but a *texture pack generator*. It can be used to *convert a low-res texture pack* into
a *hybrid* texture pack.

#### What is a hybrid texture pack?

A hybrid texture pack is a pack of low-res (8x8 to 32x32) texture pack that has every texture blended together with a
high-res texture. This way, on the first look it looks just like the normal Minecraft, but if you take a closer look,
there are the subtle details of a high-res texture packs. You can see every grain of sand and every leaf of a tree while
still being faithful to the original minecraft textures.

#### So how do I build a texture pack that looks like the screenshots?

A guide how to build this can be found below. Alternatively, you can download the pre-built texture pack for faithful32
from the Gitlab CI build. The original Minecraft textures could NOT be included in the automated build due to licensing
issues. If you want the original Minecraft textures, you need to build the pack yourself (see guide below).

#### Which mods are supported by this pack?

Currently the following mods are supported by this pack:

Name                | Version | Status
--------------------|-------|----------------------------------------------------
Minecraft          | 1.16.5 | Most blocks are supported, currently no items are supported.
Biomes O' Plenty | 13.0     | All Blocks are supported
Create            | 0.3e    | Currently only Zinc Ore is supported
Environmental     | 1.0.0   | Most blocks are supported
Forbidden & Arcanus | 16.2  | Almost all blocks supported
Powah             | 2.3.16  | Currently only Uraninite Ore is supported
Quark             | 2.4     | WIP

The CI build is based on faithful32 and therefore **does not support any mods**.

#### What if your favourite mod is not included in the game?

You can easily implement your own mod support. This generator even has full support for animated textures (though it
might look a little strange sometimes). See [this readme](CONTRIBUTING.md) for an overview how to change this script and
contribute to its development.

## Pre-built texture packs

The pre-built texture packs are to be found in the CI artifacts of this repository.
**Note that the pre-built texture packs are based upon the [Faithful](https://github.com/FaithfulTeam/Faithful) texture
pack - the original minecraft textures cannot be included here for licensing reasons.**
To play the game with the faithful-based built, you need to download and include the original faithful texture pack
before including this texture pack. If you like the Faithful pack, please consider supporting the author.

## Building a texture pack

You can build the texture packs yourself using a texture pack of your choice:

#### 1. Install dependencies

You need `Python 3.9` with `numpy`, `pillow`, `tqdm` and `blend_modes` installed.

#### 2. Clone this repository

Clone this repository and `cd` into the root folder.

#### 3. Extract assets

To extract the assets, run the bash script `extractModAssets.sh` from the root of this repository for each mod you want
to support. To extract the assets for the minecraft base game, run the script with the full path to your minecraft.jar
as argument.

##### Example:

```bash
# Change directory into the repo folder
cd minecraft-hybrid-pack

# Extract Base Game Assets
./extractModAssets.sh ~/.local/share/multimc/instances/Valhelsia\ 3\ 3.2.2/minecraft/minecraft.jar

# Extract Mod Files for Biomes o' Plenty
./extractModAssets.sh ~/.local/share/multimc/instances/Valhelsia\ 3\ 3.2.2/minecraft/mods/BiomesOPlenty-1.16.4-13.0.0.431-universal.jar
```

#### 4. Run the scripts

Run `python3 build.py`

#### 5. Zip the resource pack and include it in Minecraft

Just put the content of the `out/` folder into a zip archive and copy the zip archive into your resourcepacks folder.

## Contributing or modifying the pack generator

Detailled information how to modify the generator or add new blocks can be found [here](CONTRIBUTING.md)