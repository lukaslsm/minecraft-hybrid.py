# The blocks JSON metadata specification

The blocks JSON file consists of one huge list of dictionaries that each contain one specification ("rule") about how to
process certain blocks. Each specification needs a `"scope"` that maps to each block (list of strings) the specification
applies to, without file extension (e.g. `"oak_planks"`).

The second value each specification needs is a `"process"` that describes how each of the blocks specified in `"scope"`
should be processed.

## "process"

Each process consists of a list of operations (dictionaries) that each contain a single value (the operator) mapped to
the parameters.

### Operators

* `blend` - Blends the whole image with the given layerimage. Possible parameters are
    * `layerimage` - Name of the layerimage to blend
    * `opacity` - Opacity of the blend, as integer between 0 and 100 percent
    * `mixmode` - Mix mode, one of `normal`, `soft_light`, `hard_light`, `lighten_only`, `darken_only`, `multiply`
      , `difference`, `subtract`, `grain_extract`, `grain_merge`, `divide`. A detailled documentation of the mix modes
      can be found [here](https://github.com/flrs/blend_modes).
* `preset` - Applies a preset specified in `presets.json`.
    * `name` - Name of the preset to apply

# Mod Support

To implement support for textures of mods, you need to create one blocks json specification per mod in the `mods/`
folder. It needs to have the exact name of the mod, as it occurs in the `assets/` folder. The structure of each modded
block json file is exactly the same as in the minecraft base game blocks.json.